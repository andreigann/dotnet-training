https://github.com/aspnet/Mvc/issues/8230
https://github.com/aspnet/Mvc/issues/7753

fluentvalidation - https://fluentvalidation.net/aspnet
fluentassertion - https://fluentassertions.com/
https://github.com/FluentDateTime/FluentDateTime

mocking
    nsubstitute
    moq
    mock vs stab vs fake - https://stackoverflow.com/questions/346372/whats-the-difference-between-faking-mocking-and-stubbing
    extract a static dependency

asp core alternatives: nancy
    http://nancyfx.org/
    https://www.excella.com/insights/getting-started-nancyfx-and-asp-net-core

json 

different approaches
    mediatr
    ddd
    layered
    ports and adapters


-	dotnet authentication
        jwt - bearer token
        oauth openid
        https://security.stackexchange.com/questions/44611/difference-between-oauth-openid-and-openid-connect-in-very-simple-term
        IdentityServer4

-	dotnet angular template 
-	docker containers  
    Visual Studio Code plug-ins

automapper
    https://automapper.org/

ORMs
    EF - https://docs.microsoft.com/en-us/ef/core/
    nHibernate
        show nuget plugins in vs
    Dapper
        https://github.com/StackExchange/Dapper
        it doesn't attempt to generate your models, doesn't track schema changes for you and doesn't use auto-generated models to create dynamic SQL for you. And above all else it's magnitudes faster than the majority of full-blown ORM's out there.
    https://github.com/CollaboratingPlatypus/PetaPoco

    graphql

   sql injection

DbUp - https://dbup.github.io/



Create/update operations
Mapping from objects back to the database
Support for relationships between tables, expressed as relationships between objects
Lazy loading
SQL generation
Saving changes in batches rather than one row at a time

