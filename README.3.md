-	asp core filters / middlewares
-	asp core validation
-	asp core logging
-	HTTP client best practices
-	dotnet authentication
-	dotnet angular template 
-	docker containers  


-- aplication: calls 

    filters: https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/filters?view=aspnetcore-2.2
    middleware: https://docs.microsoft.com/en-us/aspnet/core/fundamentals/middleware/?view=aspnetcore-2.2

- parameter injection
    FromBody
    FromRoute
    FromQuery
    FromServices

fluent validation
    https://fluentvalidation.net/aspnet


    - live demo
    - enable swagger
    - enable fluent validation


logging - ILogger

HttpClient best practices
    https://aspnetmonsters.com/2016/08/2016-08-27-httpclientwrong/
    https://docs.microsoft.com/en-us/aspnet/core/fundamentals/http-requests?view=aspnetcore-2.2

authorizing API - jwt tokens (add middleware)
jwt tokens
    https://jwt.io/

authentication middlewares

dotnet angular template

docker containers



