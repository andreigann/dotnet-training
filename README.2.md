- dotnet CLI
- dotnet templates 
- dotnet authentication 
- asp pipeline, filters 
- asp core validation
https://docs.microsoft.com/en-us/aspnet/core/?view=aspnetcore-2.2

chrome debugging ????

https://docs.microsoft.com/en-us/dotnet/standard/modern-web-apps-azure-architecture/architectural-principles#explicit-dependencies

FROM SESSION1:
  xunit project template

  Create .net asp core build in integration tests
    https://docs.microsoft.com/en-us/aspnet/core/test/integration-tests?view=aspnetcore-2.2
----------------------

Tooling:
    chocolatey: dotnet 2.2 (3 in progress)
    resharper - naming conventions + show decompile


dotnet new
  create new projects from command line / using VS 2017
  additional templates are available also online (https://github.com/dotnet/templating/wiki/Available-templates-for-dotnet-new)

asp core api - template
    https://www.ageofascent.com/2019/02/04/asp-net-core-saturating-10gbe-at-7-million-requests-per-second/
    docker support: linux / windows

   create new template on the fly
      describe structure
      run using Kestrel + run using IIS - differences - enable windows authentication (show Properties windows)

        Application Startup (https://docs.microsoft.com/en-us/aspnet/core/fundamentals/startup?view=aspnetcore-2.2):
            ConfigureServices - dependency injection    - dependency injection - constructor based - small classes
            Configure - processing pipeline

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2); => different middlewares for the application

    
      describe how development works (ASPNETCORE_ENVIRONMENT)
            ASP.NET Core reads the environment variable ASPNETCORE_ENVIRONMENT
                https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-2.2

            can be set in environment variables (set ASPNETCORE_ENVIRONMENT=Development); - in docker this is the practice - overwrite config variables
            can be set in web.config
            can be set in Properties > launchSettings.json
            right click - Properties -> Debug

                Azure Key Vault
                Command-line arguments
                Custom providers (installed or created)
                Directory files
                Environment variables
                In-memory .NET objects
                Settings files

    asp core routing
            https://docs.microsoft.com/en-us/aspnet/core/fundamentals/routing?view=aspnetcore-2.2
        default is: {controller=Home}/{action=Index}/{id?}

    talk about REST verbs
    talk about attribute routing

    http://abf21a3b8f73111e894140e5a9e123b0-144068593.us-east-1.elb.amazonaws.com/swagger/index.html


--------------------------------------------------------
 cross cutitng concers: authorization, logging, exception handling
    filters: https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/filters?view=aspnetcore-2.2
    middleware: https://docs.microsoft.com/en-us/aspnet/core/fundamentals/middleware/?view=aspnetcore-2.2

    open docx
    talk about type of filters
    talk about use cases


    create exception filter
        general rule for exception: consistent handling, not many catches, throw exceptions

    show culture middle ware from startup
    create on the fly logging middleware


--------------------------------------------------------

fluent validation

--------------------------------------------------------
httpclient

https://aspnetmonsters.com/2016/08/2016-08-27-httpclientwrong/

--------------------------------------------------------

authentication / authorization
   windows authentication
   jwt token
