using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;
using Xunit.Abstractions;

namespace Session1.Tests
{
    public class ValuesControllersShould
    {
        public class BasicTests
            : IClassFixture<WebApplicationFactory<Startup>>
        {
            private readonly WebApplicationFactory<Startup> _factory;
            private readonly ITestOutputHelper _testOutputHelper;

            public BasicTests(WebApplicationFactory<Startup> factory, ITestOutputHelper testOutputHelper)
            {
                _factory = factory;
                _testOutputHelper = testOutputHelper;
            }

            [Theory]
            [InlineData("/api/values")]
            public async Task Get_EndpointsReturnSuccessAndCorrectContentType(string url)
            {
                // Arrange
                var client = _factory.CreateClient();

                // Act
                var response = await client.GetAsync(url);

                // Assert
                response.EnsureSuccessStatusCode(); // Status Code 200-299
                Assert.Equal("application/json; charset=utf-8",
                    response.Content.Headers.ContentType.ToString());

                var content = await response.Content.ReadAsStringAsync();

                _testOutputHelper.WriteLine(content);
            }
        }
    }
}
