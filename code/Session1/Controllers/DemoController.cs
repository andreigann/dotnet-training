﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Session1.Infrastructure;

namespace Session1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DemoController : ControllerBase
    {
        public ILogger<DemoController> Logger { get; }

        public DemoController(ILogger<DemoController> logger)
        {
            Logger = logger;
        }

        [HttpGet]
        [Route("[action]")]
        public bool Throw()
        {
            try
            {
                var d = 4 - 4;
                Logger.LogInformation("Executing method");
                return 1 / d > 4;
            }
            catch (Exception e)
            {
                Logger.LogError("An exception occured");
                throw;
            }
        }

        [HttpGet]
        [Route("[action]")]
        public bool ThrowEx()
        {
            try
            {
                var d = 4 - 4;
                Logger.LogInformation("Executing method");
                return 1 / d > 4;
            }
            catch (Exception e)
            {
                Logger.LogError("An exception occured");
                throw e;
            }
        }


        // Classic scenario - when devs use try catch :)
        [HttpGet]
        [Route("[action]")]
        public async Task<bool> ExceptionHandled()
        {
            var ts = new TaskCompletionSource<bool>();

            try
            {
                var names = new[] { "Name1", "Name2" };
                var name3 = names[2]; // when enabled - will stop here / disabled it will not stop
            }
            catch (Exception e)
            {
                Logger.LogError("Exception error");
                throw; // it will stop here when enabled (2nd chance)
            }

            ts.SetResult(true);

            return await ts.Task;
        }

        [HttpGet]
        [Route("[action]")]
        public async Task<bool> ExceptionNotHandled()
        {
            var ts = new TaskCompletionSource<bool>();

            var names = new[] { "Name1", "Name2" };
            var name3 = names[2];

            ts.SetResult(true);

            return await ts.Task;
        }


        //[HttpGet]
        //[Route("asyncexceptions")]
        //public async Task<bool> AsyncException()
        //{
        //    return true;
        //}

        //private async Task<bool> LongRunningProcess()
        //{
        //    await Task.Delay(TimeSpan.FromSeconds(1));

        //    return true;
        //}

        [HttpGet]
        [Route("[action]")]
        public string DeepStack()
        {
            var bll = new Bll();
            Customer customer = new Customer
            {
                Id = 10,
                Name = "Mihai"
            };
            bll.UpdateCustomer(customer);


            return "Success";
        }

        [HttpGet]
        [Route("[action]")]
        public string SubexpressionBreakpoints()
        {
            int i;
            int j;

            for (i = 0, j = 0; i < 10; i++, j++)
            {
                Trace.WriteLine(String.Format("i = {0}; j = {1}", i, j));
            }

            return "Success";
        }

        [HttpGet]
        [Route("[action]")]
        public string ConditionalBreakpoints()
        {

            int i;
            int j;
            for (i = 0, j = 0; i < 10; i++, j--)
            {
                var sb = new StringBuilder();
                sb.AppendFormat("i = {0}, j = {0}", i, j);
                Trace.WriteLine(sb.ToString());
            }


            var names = new[] { "Dan", "Andrei", "Mihai" };
            foreach (var name in names)
            {
                Trace.WriteLine(name);
                Trace.WriteLine(name);
                Trace.WriteLine(name);
            }

            return "Success";
        }

        private bool IsMyName(string name)
        {
            if (name == "Andrei")
            {
                return true;
            }
            return false;
        }

        [HttpGet]
        [Route("[action]")]
        public string HitCountBreakpoints()
        {
            var enumerableCollection = Enumerable.Range(1, 1000);

            var sum = 0;
            foreach (var item in enumerableCollection)
            {
                sum += GetFirstDigit(item);
            }

            enumerableCollection.Where(x => { return x.ToString()[0] > '2'; }).Select(x => x).ToArray();

            return "Success";
        }

        private int GetFirstDigit(int number)
        {
            var charDigit = number.ToString(CultureInfo.InvariantCulture)[0];
            if (number == 500)
            {
                throw new DemoException();
            }
            return Convert.ToInt32(charDigit);
        }


        [HttpGet]
        [Route("[action]")]
        public string FilterBreakpoints()
        {
            const int noOperations = 10;
            //var tasks = new Task[noTasks];
            var threads = new Thread[noOperations];

            for (int i = 0; i < noOperations; i++)
            {
                threads[i] = new Thread(DoWork);
                threads[i].Start();
            }

            waitHandle.Set();
                
            for (int i = 0; i < threads.Length; i++)
            {
                threads[i].Join();
            }

            return "Success";
        }


        ManualResetEvent waitHandle = new ManualResetEvent(false);
        private void DoWork()
        {
            var sb = new StringBuilder();
            sb.AppendFormat("Thread {0} starting.", GetCurrentThreadId());
            Trace.WriteLine(sb.ToString());
            waitHandle.WaitOne();
            sb.Length = 0;
            sb.AppendFormat("Thread {0} ending.", GetCurrentThreadId());
            Trace.WriteLine(sb.ToString());
        }

        private int GetCurrentThreadId()
        {
            return Thread.CurrentThread.ManagedThreadId;

        }

        [HttpGet]
        [Route("[action]")]
        public string WhenHitBreakpoints()
        {
            var sum = 0;
            foreach (var i in Enumerable.Range(1, 100))
            {
                if (i % 14 == 0)
                {
                    sum += i;
                }
            }

            return "Success " + sum;
        }
    }

}