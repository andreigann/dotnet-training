﻿using System.Diagnostics;

namespace Session1.Infrastructure
{
    [DebuggerDisplay("Customer #{Id}")]
    public class Customer
    {
        public long? Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

    }
}