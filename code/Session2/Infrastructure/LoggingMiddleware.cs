﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Session2.Infrastructure
{
    public class LoggingMiddleware
    {
        private readonly RequestDelegate _next;

        public LoggingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context, ILogger<LoggingMiddleware> logger)
        {
            logger.LogInformation($"------------------------- START {context.Request.Path} - {DateTime.Now}");
            // Call the next delegate/middleware in the pipeline
            await _next(context);
            logger.LogInformation($"------------------------- END {context.Request.Path} - {DateTime.Now}");
        }
    }


    public static class LoggingMiddlewareExtensions
    {
        public static IApplicationBuilder UsePerformanceLogging(
            this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<LoggingMiddleware>();
        }
    }
}
