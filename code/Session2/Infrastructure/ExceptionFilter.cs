﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace Session2.Infrastructure
{
    public class ExceptionFilter : IExceptionFilter
    {
        private readonly ILogger<ExceptionFilter> _logger;

        // asp core knows how to inject this
        public ExceptionFilter(ILogger<ExceptionFilter> logger)
        {
            _logger = logger;
        }
        public void OnException(ExceptionContext context)
        {
            var exception = context.Exception;
            _logger.LogError(new EventId(exception.HResult), exception, exception.Message);

            //context.Result = new BadRequestObjectResult("exception");
            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.OK; // :)
            context.ExceptionHandled = true;
        }
    }
}
