﻿using System;
using System.Dynamic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Session2.Models;

namespace Session2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DemoController : ControllerBase
    {
        private readonly IOptions<MySettings> _mySettingsOptions;
        private readonly IConfiguration _configuration;

        public DemoController(IOptions<MySettings> mySettingsOptions, IConfiguration configuration)
        {
            _mySettingsOptions = mySettingsOptions;
            _configuration = configuration;
        }

        [HttpGet]
        [Route("options")]
        public ActionResult<object> GetWithOptions()
        {
            object o =_mySettingsOptions.Value;

            //((MySettings)o).Url1;
            //dynamic d = (dynamic)o;

            //d.Url1


            return _mySettingsOptions.Value;
        }

        [HttpGet]
        [Route("configuration")]
        public ActionResult<dynamic> GetWithConfiguration()
        {
            return _configuration["MySettings:Url1"];
        }

        [HttpGet]
        [Route("exception")]
        public ActionResult<dynamic> ThrowException()
        {
            throw new Exception("Am I ok?");
        }

    }
}