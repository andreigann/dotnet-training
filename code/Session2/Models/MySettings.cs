﻿namespace Session2.Models
{
    public class MySettings
    {
        public string Url1 { get; set; }
        public AnotherSection Section { get; set; }

    }

    public class AnotherSection
    {
        public int NumberOfRequests { get; set; }
    }
}
