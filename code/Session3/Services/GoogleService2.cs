﻿using System.Net.Http;
using System.Threading.Tasks;

namespace Session3.Services
{
    public class GoogleService2
    {
        private readonly HttpClient _httpClient;

        public GoogleService2(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<string> Get()
        {
            return await _httpClient.GetStringAsync("http://www.google.com/");

        }
    }
}
