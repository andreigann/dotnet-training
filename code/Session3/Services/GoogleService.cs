﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Session3.Services
{
    public class GoogleService
    {
        private readonly HttpClient _httpClient;

        public GoogleService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        internal async Task<string> Get()
        {
            return await _httpClient.GetStringAsync("http://www.google.com/");
        }
    }
}
