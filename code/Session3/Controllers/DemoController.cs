﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Session3.Services;

namespace Session3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DemoController : ControllerBase
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly GoogleService _googleService;

        public DemoController(IHttpClientFactory httpClientFactory, GoogleService googleService)
        {
            _httpClientFactory = httpClientFactory;
            _googleService = googleService;
        }

        [HttpGet("factory")]
        public async Task<ActionResult> GetWithFactory()
        {
            var client = _httpClientFactory.CreateClient();
            var result = await client.GetStringAsync("http://www.google.com");
            return Ok(result);
        }

        [HttpGet("with-di")]
        public async Task<ActionResult> GetWithInjection()
        {
            var text = await _googleService.Get();
            return Ok(text);
        }

        [HttpGet("with-service")]
        public async Task<ActionResult> GetWithServiceInjection([FromServices] GoogleService2 googleService2)
        {
            var text = await googleService2.Get();
            return Ok(text);
        }

    }
}