Tools:
  Visual Studio Code
  Visual Studio 2017
  Chocolatey
  Postman
  Fiddler
  Conemu
  Sysinternals
  kdiff3
  Meld
  PATH: git location for linux commands
  Resharper (keep code green) (https://www.jetbrains.com/resharper/docs/ReSharper_DefaultKeymap_VSscheme.pdf)
    File structure window
    Refactoring shortcuts
    Other windows

  Always have style guides / code conventions
  Use tools that support this
  Always read the code and documentation
  Write unit tests
  Keep code clean / remove unused using; comments;

dotnet new
  create new projects from command line / using VS 2017

Properties > launchSettings.json
  different configurations

dotnet run

dotnet watch run
  change a value in project and show the output
    add a new value in ValuesController

asp core
  TIP: csproj can be edited from VS (right click)
  Application Startup (https://docs.microsoft.com/en-us/aspnet/core/fundamentals/startup?view=aspnetcore-2.2):
    ConfigureServices - dependency injection
    Configure - processing pipeline

dependency injection - constructor based - small classes

General Rules:
  enable swagger on API projects using Swashbuckle 
    https://docs.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-2.2&tabs=visual-studio
    dotnet add package Swashbuckle.AspNetCore

    allow easy method call (alternative: use Postman, fiddler)

  xunit project template

  Create .net asp core build in integration tests
    https://docs.microsoft.com/en-us/aspnet/core/test/integration-tests?view=aspnetcore-2.2

  Some words about pdbs

  Exceptions:
   throw vs throw ex; - there are 2 API methods (execute them) - show stack trace and the line number of the error
     at Session1.Controllers.DemoController.Throw() in D:\My\dotnet-training\code\Session1\Controllers\DemoController.cs:line 27
     at Session1.Controllers.DemoController.ThrowEx() in D:\My\dotnet-training\code\Session1\Controllers\DemoController.cs:line 49

     "throw" maintains the full hierarchy in the stack trace and gives complete information about the exception occurred in the code. Whereas "throw ex" pretends that exceptions occurred on the line where "throw ex" was written and removes all the hierarchy above the method containing the "throw ex" expression. 


   Clr exceptions settings - CTRL + ALT + E
    Disabled
      the debugger will not stop
    Enabled
     when enabled on exception type: the debugger will stop exactly where the exception was triggered
     if a catch was present, when pressing F5 it will stop also in catch
 
  Show Resharper stack window


  Enable Just My Code - enable debugging on libraries - WILL STOP ON EVERY EXCEPTION
    https://docs.microsoft.com/en-us/visualstudio/debugger/just-my-code?view=vs-2017
    Look at "Modules" windows - check pdbs were loaded 

    Can cause a lot of issues with debugging; only if the code is available on your machine and it is a debug build, that it is what you are allowed to debug 

    "Call stack window" - check "Show External Code"

    "Output" window
      right click and show check boxes

    "Watch" window
      Object Id (GC.GetGeneration(objectId))
      Calling a method from the watch windows will not trigger breakpoints; it will execute on the current thread (save context; execute; restore context) => 5 seconds to execute a method in watch window

    "Autos" & "Locals"

    "Immediate" Window
      show demo


    Smart breakpoints (show also Chrome - maybe console.log)
      "Breapoint" window
        console output breakpoint
        condition
        Hit count
          Finally, hit count breakpoints can also be used simply to count the number of times the breakpoint is hit. Let’s say we wanted to count the number of iterations in our GCD example. As a before, you want to create a hit count breakpoint and select the option “>=”. This time, instead of choosing a number to break on, instead choose an arbitrarily large number like 99999. 

        filter (ex: set a thread a name - and add a filter - thead name can be altered from thread window - in code can be set only once)
          show also console output to see thread running
        Deep callstack – break on the return address – in the Call Stack windows – choose breakpoint and press F9 => in Breakpoint window will appear an offset 
        Subexpression breakpoint (separated by ;): example - for loop (in breakpoint window will appear Character) – to clear the breakpoint just press in the left of the window – will be cleared one by one 
        Trace point – print out interesting methods (default is Continue Execution) 
      visual glyph
      breakpoints on call stack


    "Threads" windows
      show threads call stack
      tips and tricks to debug threads


